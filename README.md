# XPloidAssignment 1.0
Software to assign genotyped individuals to populations or genetic pools when ploidy of individuals and over genotyped loci varies, packaged for Linux and Windows.

CC-BY-NC-SA license, version 4: Solenn Stoeckel, INRAE, 2016.

Montecinos, A.E., Guillemin, M.-L., Couceiro, L., Peters, A.F., Stoeckel, S. and Valero, M. (2017), Hybridization between two cryptic filamentous brown seaweeds along the shore: analysing pre- and postzygotic barriers in populations of individuals with varying ploidy levels. Mol Ecol, 26: 3497-3512. https://doi.org/10.1111/mec.14098

# How to use
1. Download the packaged file for your operating system and unzipped. All files included documentation and example formatted files are in the unzipped folder.
2. Change permission if needed. In Gnu-Linux system, right-click on the executable file and in the tab 'permission', tick 'allow execute file', or in command-line, enter _chmod o+rwx XPloidAssignment1.0.
3. Launch the XPloidAssignment executable file (exe for windows and x-executable for Linux)
4. Read the documentation.

# Know issue
None

# Contact: 
 	Solenn Stoeckel

  	UMR DECOD (Ecosystem Dynamics and Sustainability), INRAE-Institut Agro-IFREMER,

  	Bât. 19, 65, rue de Saint-Brieuc, CS 84215, 35042 Rennes Cedex, France

  	solenn.stoeckel@inrae.fr 

https://forgemia.inra.fr/solenn.stoeckel

https://scholar.google.com/citations?user=9_ngu_IAAAAJ

https://cv.hal.science/solenn-stoeckel?langChosen=fr


## Last update: 14 March 2024, Solenn Stoeckel.



